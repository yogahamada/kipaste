<?php
session_start();

require('inc/config.php');
require('phpseclib/Crypt/AES.php');
require('geshi/geshi.php');

$error = array();
$curTime = time();
$id = strtoupper(sha1(md5(uniqid())));
$data['id'] = substr($id, 0, 10); 

if(TOKEN)
{
	if(isset($_POST['token']) && isset($_SESSION['token']) && $_SESSION['token'] == $_POST['token'])
	{
		unset($_SESSION['token']);
	}
	else
		$error[] = "Token invalide.";
}

if(CAPTCHA)
{
	if(isset($_POST['captcha']) && isset($_SESSION['captcha']) && $_SESSION['captcha'] == $_POST['captcha'])
	{
		unset($_SESSION['captcha']);
	}
	else
		$error[] = "Captcha invalide.";
}

if(isset($_POST['titre']) && !empty($_POST['titre']) && strlen($_POST['titre'])<=65 && is_string($_POST['titre']))
{
	$data['titre'] = htmlspecialchars($_POST['titre']);
}
else
	$error[] = 'Titre invalide';


if(isset($_POST['auto-burn']) || (isset($_POST['expiration']) && !empty($_POST['expiration'])))
{
	
	if(!isset($_POST['auto-burn']) || $_POST['auto-burn']!="on")
	{
		switch ($_POST['expiration']) 
		{
			case '1H':
				$data['expiration'] = $curTime + 3600; // temps actuel plus un jour
				break;
			case '1D':
				$data['expiration'] = $curTime + 3600 * 24; // temps actuel plus un jour
				break;
			case '1W':
				$data['expiration'] = $curTime + 3600 * 24 * 7; // temps actuel plus 7 jours
				break;
			case '2W':
				$data['expiration'] = $curTime + 3600 * 24 * 14; // temps actuel plus 14 jours
				break;
			case '1M':
				$data['expiration'] = $curTime + 3600 * 24 * 30; // temps actuel plus 30 jours
				break;
			case '1Y':
				$data['expiration'] = $curTime + 3600 * 24 * 365; // temps actuel plus 365 jours
				break;
			case 'NO':
				$data['expiration'] = 0; 
				break;
			default:
				$error[] = "Date d'expiration invalide";
				break;
		}
	}
	elseif(isset($_POST['auto-burn']) && $_POST['auto-burn']=="on")
	{
		$data['expiration'] = 1;
	}
	else
		$error[] = "Date d'expiration invalide";
}
else
	$error[] = "Date d'expiration vide ou non reçue";



if(isset($_POST['coloration']) && !empty($_POST['coloration']))
{
	if(file_exists('geshi/geshi/' . $_POST['coloration'] . '.php'))
		$data['highlighter'] = $_POST['coloration'];
	else
		$error[] = "Type de coloration syntaxique invalide";

}
else
	$error[] = "Type de coloration syntaxique invalide";



if(isset($_POST['protect']) && !empty($_POST['protect']))
{


	switch ($_POST['protect']) 
	{
		case 'off':
			$data['allow']['type'] = 'off';
			break;
		case 'ip':

			if(isset($_POST['ipAllow']))
			{
				$data['allow']['type'] = 'ip';
				$ips = $_POST['ipAllow'];
				$ips = preg_split("#,#", $ips);
				foreach ($ips as &$x)
				{
					$x = trim($x);
					if(filter_var($x, FILTER_VALIDATE_IP))
						$data['allow'][] = hash('sha256', $x); // on ajoute l'adresse ip hashée en sha256 à celles autorisées
					else
					{
						$error[] = "Les adresses Ip indiquées doivent correspondrent à la norme IPv4 ou IPv6 et être séparées par des virgules.";
						break 2; // on sort du foreach et du switch
					}
				} 

					
			}
			else
				$error[] = "Paramètres de restrictions vides ou non reçus";

			break;
		case 'pass':
			$data['allow']['type'] = "pass";

			$data['allow']['pass'] = hash('sha256', $_POST['passAllow']);
			break;
		
		default:
			$data['allow']['type'] = "none";
			break;
	}
}
else
	$error[] = "Paramètres de restrictions vides ou non reçus";

if(isset($_POST['encrypt']))
{
	if($_POST['encrypt'] == "on")
		$crypt = true;
	else
		$crypt = false;
}
else
	$error[] = "Vous devez préciser si le Paste doit-être chiffré ou non.";

if(isset($_POST['contenu']) && !empty($_POST['contenu']))
{      
       if(is_string($_POST['contenu']))
               $data['contenu'] = $_POST['contenu'];
       else
               $error[] = "Moi je mange que des strings, na !";
}
else
	$error[] = "Le but de cet outil est de partager <b>du contenu</b> quand même, je tiens à le rappeler...";


if(empty($error))
{
	/*
	A partir de là on va pouvoir passer au contenu intéressant.
	Création des fichiers liés ("<id>-<expiration>.txt" & "<id>-<expiration>") ; 
	l'un contiendra les informations telles que la coloration syntaxique, 
	les restrictions d'accès (encodé en json) et l'autre contiendra le corps du message, chiffré.
	*/
	//json_encode($data)

	$Finfo = fopen("data/".$data['id']."-".$data['expiration'].".txt", "x+");
	$Fcontent = fopen("data/".$data['id']."-".$data['expiration'], "x+");

	if($Finfo && $Fcontent)
	{
		$infos = array();

		$infos['titre'] = $data['titre'];
		$infos['protect'] = $data['allow'];
		$infos['langage'] = $data['highlighter'];

		if(fwrite($Finfo, json_encode($infos)))
		{
			if($crypt) // on chiffre le fichier comme demandé
			{
				$cipher = new Crypt_AES(); // could use CRYPT_AES_MODE_CBC
				// keys are null-padded to the closest valid size
				// longer than the longest key and it's truncated
				$cipher->setKeyLength(256);
				$key = md5(bin2hex(openssl_random_pseudo_bytes(256)));
				$cipher->setKey($key);
				//$cipher->setIV('...'); // defaults to all-NULLs if not explicitely defined

				$encrypted = $cipher->encrypt($data['contenu']);
			}
			else // on ne chiffre pas le fichier
			{
				$encrypted = $data['contenu']; // tout bêtement :')
			}

			if(fwrite($Fcontent, $encrypted) === FALSE)
				$error[] = "Le contenu de votre Paste n'a pu être enregistré, veuillez réessayer.";
		}
		else
			$error[] = "Les fichier contenant les informations relatives au Paste n'a pu être enregistré, veuillez réessayer.";



		fclose($Finfo);
		fclose($Fcontent);
	}
	else
		$error[] = "Les données n'ont pu êtres enregistrées, veuillez réessayer.";
}

if(!empty($error)) // Huston, nous avons un problème !
{
	$_SESSION['error'] = $error;
	$_SESSION['contenu'] = $_POST['contenu'];
	//echo '<h1>Erreur !</h1>';
	if(!isset($_POST['api']))
		header('location:index.php');
	else
		print_r($error);
}
else
{
	if(!isset($_POST['api']))
	{
		$title = "securePaste - Success";


		include('inc/header.php');
		?>
		<div class="alert alert-success">
	    <b>Oh Yeeaahh ! \o/</b>
	    <p>Votre Paste à été crée avec succès. Vous le trouverez à cette adresse (Ctrl+C) : <br />
	    	<input type="text" class="input-xxlarge givelink autoselect" value="<?php echo getUrl($crypt, $data['id'], @$key); ?>" class=""><br />
	    	<span class="label label-warning">Attention</span> Ne perdez pas cette adresse car elle seule permet d'accéder à votre Paste et de le déchiffrer.</p>
	    </div>
	   
	    <div class="span11" id="main">
	    	 <span style="float:right;display:block;">
				<a href="<?php echo WEBSITE; ?>" class="btn btn-large"><i class="icon-pencil"></i> Créer un nouveau Paste</a>
			</span>

	    	<h3><?php echo $data['titre'].' - #'.$data['id']; ?></h3>
	    	<?php
	    		$geshi = new GeSHi($data['contenu'], $data['highlighter']);
	    		$geshi->enable_line_numbers(GESHI_FANCY_LINE_NUMBERS);
				echo $geshi->parse_code();
	    	?>
	    
	    </div>
	    <script>
	    $(document).ready(function(){
	    	
	    	$(".autoselect").select();
	        
	    });
	    </script>
		<?php
		include('inc/footer.php');
	}
	else
	{
		$result = array("id" => $data['id']);

		if(isset($crypt) && isset($key))
			$result["key"] = $key;

		if(isset($_POST['raw']) && $_POST['raw'])
			$result['raw'] = 1;
		else
			$result['raw'] = 0;

		print(json_encode(print_r($result)));
	}
}
