# securePaste

securePaste est un système de gestion de Paste sécurisé et open-source. Tous les Pastes peuvent être  chiffrés en utilisant AES 256bits et vous avez la possibilité de définir des conditions afin de restreindre l'accès à votre Paste à des adresses IP prédéfinies ou encore la bonne vieille protection par mot de passe.

securePaste utilise [Bootstrap](http://twitter.github.io/bootstrap/) 
pour le design, 
[GeSHi](http://qbnz.com/highlighter/) pour la coloration syntaxique, 
ainsi que 
[phpseclib](http://phpseclib.sourceforge.net/) pour le chiffrement des 
données.

## Principe de fonctionnement de securePaste
Lors de la création d'un nouveau Paste par un utilisateur lanbda, 
celui-ci est ammené à définir plusieurs variables en plus du contenu 
dudit Paste :

- Le type de coloration syntaxique
- La date d'expiration du Paste
- Le type de restrictions appliqué


Dès la validation du formulaire, 2 fichiers sont crées dans le 
répertoire *data/* : 

- Le premier fichier, non chiffré contient toutes les informations 
concernant les 3 paramètres cités plus haut, le tout encodé en Json
- Le second contient les données du Paste, chiffrées symétriquement avec 
une clef dont seul l'utilisateur a connaissance.

Les 2 fichiers crées portent un nom quasi identique ; tous deux ont pour 
nom l'id du paste généré ainsi que le timestamp d'expiration de ce 
dernier.
Ce système permet de faciliter le travail de suppression des pastes 
expirés qui se fait via une tache cron (éxécutée toutes les heures), 
sélectionnant tous les fichiers dont le timestamp (qui est inscrit dans 
le nom) est inferieur au timestamp actuel.

# Getting started

	git clone https://Popoliito@bitbucket.org/Popoliito/securepaste.git
	cd securepaste/
	mkdir data/
	touch captcha.png
	chmod -R 777 data/
	chmod 777 captcha.png


Et voilà, c'est tout ! :-)

Si vous constatez un bug ou que vous avez un quelquonque problème, n'hésitez pas à m'envoyer un mail à boss[at]samtala[dot]eu
